import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import org.testng.xml.dom.Tag;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by @author Artem Morozov on 7/18/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Salesforce {
    private WebDriver driver;
    private SalesforcePageFactory pf;
    private WebDriverWait wait;
    private final Modules modules = new Modules();
    private JavascriptExecutor jse;
    private SoftAssert softAssert;
    private ExtentReports reports;
    private ExtentTest logReport;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        pf = new SalesforcePageFactory(driver);
        jse = (JavascriptExecutor) driver;
        softAssert = new SoftAssert();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.setProperty("webdriver.chrome.driver", "/Users/morozov_artem/chromedriver");
        reports = new ExtentReports(System.getProperty("user.dir") + "//src/main/Reports/DupeDive/TestReport_" +
                new SimpleDateFormat("yyyy_MM_dd").format(new Date()) + ".html");
        logReport = reports.startTest("Automated test of Dupe Dive application in Salesforce");
    }

    @Test @Tag(name = "Data Insertion")
    public void test_add_contacts() throws Exception {
        driver.get("https://login.salesforce.com/");
        //driver.manage().addCookie(new Cookie("salesforce", "[*.]salesforce.com"));
        Thread.sleep(2000);

        pf.username.sendKeys(GetCredentials.getEmail());
        pf.password.sendKeys(GetCredentials.getPswd());
        pf.login.click();
        modules.printAndLog_PassResult(logReport, "Logged in successfully.");
        wait.until(ExpectedConditions.urlContains("lightning/page/home"));
        modules.printAndLog_PassResult(logReport, "Home page is loaded.");

        pf.iconWaffle.click();
        softAssert.assertEquals("https://na123.lightning.force.com/lightning/page/home", driver.getCurrentUrl(),
                "URL is not correct");
        pf.searchApps.sendKeys("Dupe Dive");

        if (driver.findElement(By.xpath("//a[@id='DupeDive__DupeDive']")).isDisplayed()) {
            jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@id='DupeDive__DupeDive']")));
            modules.printAndLog_PassResult(logReport, "Selected needed application from search.");
        }
        else modules.printAndLog_FailResult(logReport, "Error occurred in selecting needed app!", driver);

        wait.until(ExpectedConditions.urlContains("lightning.force.com/lightning/n/DupeDive__DupeDive"));
        modules.printAndLog_PassResult(logReport, "Application page is loaded.");
        Thread.sleep(4000);
        softAssert.assertEquals("Dupe Dive | Salesforce", driver.getTitle());

        int count = 0;
        String phone1 = "603-603-60-60", phone2 = "512-512-51-51", phone3 = "630-630-63-63", phone4 = "800-008-08-08";
        String firstName1 = "Elon", firstName2 = "Joel", firstName3 = "Artem", firstName4 = "Spencer";
        String lastName1 = "Musk", lastName2 = "Zimmerman", lastName3 = "Stolyarov", lastName4 = "Brown", lastName5 = "NoName";

        wait.until(ExpectedConditions.elementToBeClickable(pf.contactsDropdown));
        Thread.sleep(3000);
        pf.contactsDropdown.click();
        wait.until(ExpectedConditions.elementToBeClickable(pf.addNewContact_dropdown));
        jse.executeScript("arguments[0].click();", pf.addNewContact_dropdown);
        wait.until(ExpectedConditions.visibilityOf(pf.newContactWindow_Footer));
        modules.printAndLog_PassResult(logReport, "Started adding a new user.");

        while (count <= 11) {
            wait.until(ExpectedConditions.visibilityOf(pf.newContactWindow_Salutation));
            wait.until(ExpectedConditions.elementToBeClickable(pf.newContactWindow_Salutation));
            pf.newContactWindow_Salutation.click();
            try {
                pf.newContactWindow_Salutation_Mr.click();
            } catch (Exception exception) {
                modules.printAndLog_FailResult(logReport, "Selecting_dropdown_error", driver);
            }

            if (count % 2 == 0) {
                if (count % 7 == 0) modules.fillContactInfo(pf, phone1, firstName2, lastName3);
                else if (count % 3 == 0) modules.fillContactInfo(pf, phone4, firstName3, lastName2);
                else modules.fillContactInfo(pf, phone1, firstName1, lastName1);
            } else if (count % 3 == 0) {
                if (count % 5 == 0) modules.fillContactInfo(pf, phone2, firstName3, lastName4);
                else modules.fillContactInfo(pf, phone2, firstName2, lastName2);
            } else if (count % 5 == 0) {
                modules.fillContactInfo(pf, phone3, firstName3, lastName3);
            } else if (count % 7 == 0) {
                modules.fillContactInfo(pf, phone4, firstName4, lastName4);
            } else if (count % 11 == 0) {
                pf.newContactWindow_LastName.sendKeys(lastName5);
                pf.newContactWindow_Save.click();
                modules.printAndLog_PassResult(logReport, "Added last user in the loop #" + count + ".");
                break;
            }
            try {
                pf.newContactWindow_SaveAndNew.click();
                modules.printAndLog_PassResult(logReport, "Added new user #" + count + ".");
            } catch (ElementClickInterceptedException e) {
                WebElement noThanks = driver.findElement(By.xpath("//button[@class='slds-button noThanksButton']"));
                noThanks.click();
                modules.printAndLog_PassResult(logReport, "Closed pop-up.");
                continue;
            }
            Thread.sleep(4000);

            count++;
        }
    }

    @Test
    public void test_Salesforce_App() throws Exception {
        driver.get("https://login.salesforce.com/");
        //driver.manage().addCookie(new Cookie("salesforce", "[*.]salesforce.com"));
        Thread.sleep(2000);

        pf.username.sendKeys(GetCredentials.getEmail());
        pf.password.sendKeys(GetCredentials.getPswd());
        pf.login.click();
        modules.printAndLog_PassResult(logReport, "Logged in successfully.");
        wait.until(ExpectedConditions.urlContains("lightning/page/home"));
        modules.printAndLog_PassResult(logReport, "Home page is loaded.");

        wait.until(ExpectedConditions.visibilityOf(pf.iconWaffle));
        softAssert.assertEquals("https://na123.lightning.force.com/lightning/page/home", driver.getCurrentUrl(),
                "URL is not correct");

        pf.iconWaffle.click();
        wait.until(ExpectedConditions.visibilityOf(pf.waffleSearchBar));
        modules.printAndLog_PassResult(logReport, "Opened apps search.");
        pf.searchApps.sendKeys("Dupe Dive");
        modules.printAndLog_PassResult(logReport, "Sent app name string.");

        if (pf.searchApps_DupeDiveOption.isDisplayed()) {
            wait.until(ExpectedConditions.visibilityOf(pf.waffleMenuList));
            jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@id='DupeDive__DupeDive']")));
            modules.printAndLog_PassResult(logReport, "Selected needed application from search.");
        } else modules.printAndLog_FailResult(logReport, "Error occurred in selecting needed app!", driver);

        wait.until(ExpectedConditions.urlContains("lightning.force.com/lightning/n/DupeDive__DupeDive"));
        Thread.sleep(6000);
        softAssert.assertEquals("Dupe Dive | Salesforce", driver.getTitle());
        modules.printAndLog_PassResult(logReport, "Loaded app's page.");

        driver.switchTo().defaultContent();
        WebElement frame = driver.findElement(By.xpath("//iframe[@title='accessibility title']"));
        driver.switchTo().frame(frame);
        modules.printAndLog_PassResult(logReport, "Switched to app's iframe.");

        modules.isDisplayed(pf.DupeDive_Home, "pf.DupeDive_Home", logReport, driver);
        modules.isDisplayed(pf.DupeDive_Settings, "pf.DupeDive_Settings", logReport, driver);

        wait.until(ExpectedConditions.visibilityOf(pf.DupeDive_wrapper));
        modules.isDisplayed(pf.DupeDiveLogo, "pf.DupeDiveLogo", logReport, driver);
        modules.isDisplayed(pf.DupeDive_leadToLead, "pf.DupeDive_leadToLead", logReport, driver);
        modules.isDisplayed(pf.DupeDive_contactToContact, "pf.DupeDive_contactToContact", logReport, driver);
        modules.isDisplayed(pf.DupeDive_accountToAccount, "pf.DupeDive_accountToAccount", logReport, driver);

        modules.compareCssColor(pf.DupeDive_contactToContact_footer, "rgba(0, 114, 191, 1)"); //assertion for color accuracy
        modules.printAndLog_PassResult(logReport, "Color of contact to contact bubble is correct (e.g. active).");
        modules.compareCssColor(pf.DupeDive_accountToAccount_footer, "rgba(0, 114, 191, 1)"); //assertion for color accuracy
        modules.printAndLog_PassResult(logReport, "Color of account to account bubble is correct (e.g. active).");

        pf.DupeDive_accountToAccount.click();
        modules.printAndLog_PassResult(logReport, "Disabled account to account bubble.");
        modules.compareCssColor(pf.DupeDive_accountToAccount_footer, "rgba(234, 234, 234, 1)"); //assertion for color accuracy after click
        modules.printAndLog_PassResult(logReport, "Color of account to account bubble is correct (e.g. not active).");

        modules.compareCssColor(pf.DupeDive_leadToLead_footer, "rgba(0, 114, 191, 1)"); //assertion for color accuracy
        modules.printAndLog_PassResult(logReport, "Color of lead to lead bubble is correct (e.g. active).");
        pf.DupeDive_leadToLead.click();
        modules.printAndLog_PassResult(logReport, "Disabled account to account bubble.");
        modules.compareCssColor(pf.DupeDive_leadToLead_footer, "rgba(234, 234, 234, 1)"); //assertion for color accuracy after click
        modules.printAndLog_PassResult(logReport, "Color of lead to lead bubble is correct (e.g. not active).");

        pf.DupeDive_scanCheckbox.click();
        modules.printAndLog_PassResult(logReport, "Selected a checkbox to scan first 10k records.");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            pf.DupeDive_scanRecords.click();
        } catch (ElementClickInterceptedException e) {
            WebElement noThanks = driver.findElement(By.xpath("//button[@class='slds-button noThanksButton']"));
            noThanks.click();
            modules.printAndLog_PassResult(logReport, "Closed pop-up.");
        }
        String dateOfScan = dateFormat.format(new Date());
        modules.printAndLog_PassResult(logReport, "Started scanning.");
        modules.isDisplayed(pf.DupeDive_scanningPercentage, "pf.DupeDive_scanningPercentage", logReport, driver);

        while (!pf.DupeDive_scanningPercentage.getText().equals("100%")) {
            if (!pf.DupeDive_scanningPercentage.getText().equals("100%")) {
                Thread.sleep(750);
                modules.printAndLog_PassResult(logReport, "Scanning is in progress: "
                        + pf.DupeDive_scanningPercentage.getText());
            } else  if (pf.DupeDive_scanningPercentage.getText().equals("100%")) {
                modules.printAndLog_PassResult(logReport, "Scanning completed: "
                        + pf.DupeDive_scanningPercentage.getText());
                break;
            }
        }

        pf.DupeDive_viewResults.click();
        modules.isDisplayed(pf.DupeDive_ScanResults, "pf.DupeDive_ScanResults", logReport, driver);
        modules.isDisplayed(pf.DupeDive_selectContactDate, "pf.DupeDive_selectContactDate", logReport, driver);
        modules.printAndLog_PassResult(logReport, "Viewing results of the scan.");

        softAssert.assertEquals(pf.DupeDive_selectContactDate_selected.getText(), dateOfScan);
        modules.printAndLog_PassResult(logReport, "Date of the scan is correct.");

        jse.executeScript("arguments[0].scrollIntoView(true);", pf.DupeDive_Contact_RiskLevel);
        String riskLevelAttribute = pf.DupeDive_Contact_RiskLevel.getAttribute("class");

        if ("riskLevelDivRed".equals(riskLevelAttribute)) {
            modules.printAndLog_PassResult(logReport, "Risk level is High with "
                    + pf.DupeDive_Contact_duplicatesNumber.getText() + " found.");
        } else if ("riskLevelDivOrange".equals(riskLevelAttribute)) {
            modules.printAndLog_PassResult(logReport, "Risk level is Medium with "
                    + pf.DupeDive_Contact_duplicatesNumber.getText() + " found.");
        } else if ("riskLevelDivGreen".equals(riskLevelAttribute)) {
            modules.printAndLog_PassResult(logReport, "Risk level is Low with "
                    + pf.DupeDive_Contact_duplicatesNumber.getText() + " found.");
        }
    }

    @After
    public void cleanup() {
        reports.endTest(logReport);
        reports.flush();
        driver.quit();
    }
} // end of Salesforce