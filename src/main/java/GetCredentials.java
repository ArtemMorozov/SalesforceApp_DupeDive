import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by @author Artem Morozov on 7/18/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class GetCredentials {
    private static String email;
    private static String pswd;

    private static void getSecrets() {
        StringBuilder sb = new StringBuilder();
        try {
            //TODO: find a way to store it more secured
            Scanner file = new Scanner(new File("secrets.txt"));
            /* Rules for .txt:
               below parser recognizes text by splitting it with space regex
               have your .txt with given format in a single line "phonenumber password" */
            while (file.hasNext()) {
                String[] secrets = file.nextLine().split(" ");
                email = sb.append(secrets[0]).toString();
                sb.setLength(0);
                pswd = sb.append(secrets[1]).toString();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String getEmail() {
        getSecrets();
        return email;
    }

    public static String getPswd() {
        getSecrets();
        return pswd;
    }
} // end of GetCredentials

/* json option:
   JSONParser parser=new JSONParser();
   Object obj = parser.parse(new FileReader("./credentials.json"));
   JSONObject jsonObject = (JSONObject) obj;
   username = (String) jsonObject.get("username");
   password = (String) jsonObject.get("password"); */