import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by @author Artem Morozov on 7/18/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class SalesforcePageFactory {
    WebDriver driver;

    public SalesforcePageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='username']")
    WebElement username;

    @FindBy(xpath = "//*[@id='password']")
    WebElement password;

    @FindBy(xpath = "//*[@id='Login']")
    WebElement login;

    @FindBy(xpath = "//div[@class='slds-icon-waffle']")
    WebElement iconWaffle;

    @FindBy(xpath = "//*[@class='searchBar slds-p-horizontal--small slds-p-top--small slds-form-element']")
    WebElement waffleSearchBar;

    @FindBy(xpath = "//div[@class='slds-size_medium']")
    WebElement waffleMenuList;

    @FindBy(xpath = "//input[@class='slds-input']")
    WebElement searchApps;

    @FindBy(xpath = "//a[@id='DupeDive__DupeDive']")
    WebElement searchApps_DupeDiveOption;

    @FindBy(xpath = "//label[@class='duplicate-scanner']")
    WebElement DupeDiveLogo;

    @FindBy(xpath = "//div[@class='row bottom-Page']")
    WebElement DupeDive_wrapper;

    @FindBy(xpath = "//span[contains(text(), 'Contacts Menu')]/..")
    WebElement contactsDropdown;

    @FindBy(xpath = "//span[contains(text(), 'New Contact')]")
    WebElement addNewContact_dropdown;

    //****** New Contact Window ******
    @FindBy(xpath = "//div[@class='inlineFooter']")
    WebElement newContactWindow_Footer;

    @FindBy(xpath = "//div[@class='slds-form slds-form_stacked slds-is-editing']/div[1]/div[2]/div//input[@class=' input'][@type='tel']")
    WebElement newContactWindow_Phone;

    //***** Salutation dropdown *****
    @FindBy(xpath = "//div[@class='salutation compoundTLRadius compoundTRRadius compoundBorderBottom form-element__row uiMenu']//a[@class='select']")
    WebElement newContactWindow_Salutation;

    @FindBy(xpath = "//a[@title='Mr.']")
    WebElement newContactWindow_Salutation_Mr;
    //***** Salutation dropdown End *****

    @FindBy(xpath = "//input[@class='firstName compoundBorderBottom form-element__row input']")
    WebElement newContactWindow_FirstName;

    @FindBy(xpath = "//input[@class='lastName compoundBLRadius compoundBRRadius form-element__row input']")
    WebElement newContactWindow_LastName;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral uiButton--neutral uiButton forceActionButton'][@title='Save & New']")
    WebElement newContactWindow_SaveAndNew;

    @FindBy(xpath = "//button[@class='slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']")
    WebElement newContactWindow_Save;
    //****** New Contact Window End ******

    //***** Dupe Dive *****
    @FindBy(xpath = "//a[@href='#home']")
    WebElement DupeDive_Home;

    @FindBy(xpath = "//a[@href='#overview-']")
    WebElement DupeDive_ScanResults;

    @FindBy(xpath = "//a[@href='#support']")
    WebElement DupeDive_Settings;

    @FindBy(xpath = "//div[@id='lead_to_lead_div']")
    WebElement DupeDive_leadToLead;

    @FindBy(css = "label#ltl_info_label.option-bottom")
    WebElement DupeDive_leadToLead_footer;

    @FindBy(xpath = "//div[@id='contact_to_contact_div']")
    WebElement DupeDive_contactToContact;

    @FindBy(css = "label#ctc_info_label.option-bottom")
    WebElement DupeDive_contactToContact_footer;

    @FindBy(xpath = "//div[@id='account_to_account_div']")
    WebElement DupeDive_accountToAccount;

    @FindBy(css = "label#ata_info_label")
    WebElement DupeDive_accountToAccount_footer;

    @FindBy(xpath = "//input[@id='isFullScan']")
    WebElement DupeDive_scanCheckbox;

    @FindBy(xpath = "//button[@id='btnScanNow']")
    WebElement DupeDive_scanRecords;

    @FindBy(xpath = "//h3[@id='scanningPercentage']")
    WebElement DupeDive_scanningPercentage;

    @FindBy(xpath = "//button[@class='btn primaryButton']")
    WebElement DupeDive_viewResults;

    @FindBy(xpath = "//select[@id='contactDateOption']")
    WebElement DupeDive_selectContactDate;

    @FindBy(xpath = "//select[@id='contactDateOption']/option[@selected='selected']")
    WebElement DupeDive_selectContactDate_selected;

    @FindBy(xpath = "//div[@id='contactDupeRiskLevel']/div")
    WebElement DupeDive_Contact_RiskLevel;

    @FindBy(xpath = "//div[@id='contactDupeCount']//p[@class='dupes-found-number']")
    WebElement DupeDive_Contact_duplicatesNumber;
    //***** Dupe Dive End*****
} // end of PageFactory