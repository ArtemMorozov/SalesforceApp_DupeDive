import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by @author Artem Morozov on 7/18/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Modules {
    /** Method used for automated filling of information for Salesforce contact
     * @param pf Page Factory for WebElements
     * @param number Phone number String
     * @param first First name String
     * @param last Last name String */
    public void fillContactInfo(SalesforcePageFactory pf, String number, String first, String last) {
        pf.newContactWindow_Phone.sendKeys(number);
        pf.newContactWindow_FirstName.sendKeys(first);
        pf.newContactWindow_LastName.sendKeys(last);
    }

    /** Method for verifying correct color of the element
     * @param element WebElement used for checking current color by CSS value
     * @param expected String with expected color */
    public void compareCssColor(WebElement element, String expected) {
        String color = element.getCssValue("background-color").trim();
        try {
            assertEquals("Color of '" + element.getText() + "' is wrong!", color, expected);
            System.out.println("Color of '" + element.getText() + "' is correct.");
        } catch (AssertionError ae) {
            ae.printStackTrace();
        }
    }

    /** Method for verifying that element is displayed. If yes that will be logged in HTML report, otherwise Exception
     * will be thrown and logged as Fail of the test
     * @param element WebElement to check
     * @param message String used to log what element this method is interacting with
     * @param et Variable for logging results
     * @param driver WebDriver */
    public void isDisplayed(WebElement element, String message, ExtentTest et, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(element));
            element.isDisplayed();
            printAndLog_PassResult(et, "Element " + message + " is displayed and selected.");
        } catch (TimeoutException te) {
            printAndLog_FailResult(et, "Element " + message + " is not displayed", driver);
            te.printStackTrace();
            driver.quit();
        } catch (Exception e) {
            printAndLog_FailResult(et, "Element " + message + " is not displayed", driver);
            e.printStackTrace();
        }
    }

    /** Method for logging PASS results
     * @param logReport Variable for logging results
     * @param message String to be shown in HTML report */
    public void printAndLog_PassResult(ExtentTest logReport, String message) {
        logReport.log(LogStatus.PASS, message);
        System.out.println(message);
    }

    /** Method for logging FAIL results with attaching of screenshot on failure using getScreenshot method
     * @param logReport Variable for logging results
     * @param message String used to log what element this method is interacting with. It is a part of screenshot name as well
     * @param driver WebDriver */
    public void printAndLog_FailResult(ExtentTest logReport, String message, WebDriver driver) {
        try {
            String img = getScreenshot(driver, message + "-");
            logReport.log(LogStatus.FAIL, message);
            logReport.log(LogStatus.INFO, "Screenshot of failure:" + logReport.addScreenCapture(img));
            System.out.println(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** Method to take a screenshot on failure saved in specified folder
     * @param driver WebDriver
     * @param screenshotName Specified file name in printAndLog_FailResult method
     * @return Returns String with a path to screenshot to be shown in HTML reporter
     * @throws Exception In case path does not exists */
    public static String getScreenshot(WebDriver driver, String screenshotName) throws Exception {
        String dateName = new SimpleDateFormat("yyyy_MM_dd_hh-mm-ss").format(new Date());
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String destination = System.getProperty("user.dir") + "//src/main/Screenshots/"
                + screenshotName + dateName + ".png";
        File finalDestination = new File(destination);
        FileUtils.copyFile(source, finalDestination);

        return destination;
    }
} // end of Modules