# Salesforce DupeDive App
### Description
A framework for testing out the Salesforce app DupeDive for identifying duplicates in contacts.

For completing the task used Java, Selenium and JUnit based framework.

Made by <a href="https://www.linkedin.com/in/morozovartem/">Artem Morozov</a>.